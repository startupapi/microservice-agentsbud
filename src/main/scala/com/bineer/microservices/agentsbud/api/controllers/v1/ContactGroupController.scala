package com.bineer.microservices.agentsbud.api.controllers.v1

import javax.inject.Inject
import akka.http.scaladsl.server.Directives.{path, get => getMethod, _}
import akka.http.scaladsl.server._
import akka.pattern.ask
import com.bineer.microservices.agentsbud.application.services.ContactGroupService
import com.bineer.microservices.agentsbud.application.services.ContactGroupServiceProtocol._
import com.bineer.microservices.agentsbud.domain.util.ContactGroupParameter
import com.bineer.microservices.agentsbud.infrastructure.mapper.ContactGroupMapper
import com.scalakka.microservices.kernel.annotation.Named
import com.scalakka.microservices.kernel.api._
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.DataIdentity
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import org.springframework.data.domain.Page

class ContactGroupController @Inject()(@Named(classOf[ContactGroupService]) serviceFactory: ActorFactory)
  extends ResourceController {

  override def post: Route = Action.postAsync {
    (data: ContactGroupParameter, _) =>
      (serviceFactory() ? Save(data)).mapResult[DataIdentity]
  }

  override def put: Route = Action.putAsync {
    (id: String, data: ContactGroupParameter, _) =>
      (serviceFactory() ? Update(id, data)).mapResult[DataIdentity]
  }

  override def delete: Route = Action.deleteAsync {
    (id: String, _) =>
      (serviceFactory() ? Delete(id)).mapResult[DataIdentity]
  }

  override def getById: Route = Action.getAsync {
    (id: String, r: Request) =>
      (serviceFactory() ? FindById(id, r.pageRequest)).mapResult[Option[ContactGroupMapper]]
  }

  override def getAll: Route = Action.getAsync {
    (r: Request) =>
      (serviceFactory() ? FindAll(r.pageRequest)).mapResult[Page[ContactGroupMapper]]
  }

  def getAllByUserId: Route = Action.custom {
    path(LongNumber) {
      userId: Long =>
        getMethod {
          new ActionComplete().completeAsync {
            (r: Request) =>
              (serviceFactory() ? FindAllByUserId(userId, r.pageRequest)).mapResult[Page[ContactGroupMapper]]
          }
        }
    }
  }

  override def routes: Route = getAllByUserId ~ super.routes
}