package com.bineer.microservices.agentsbud.api.controllers.v1

import javax.inject.Inject

import akka.http.scaladsl.server._
import akka.pattern.ask
import com.bineer.microservices.agentsbud.application.services.ContactService
import com.bineer.microservices.agentsbud.application.services.ContactServiceProtocol._
import com.bineer.microservices.agentsbud.domain.util.ContactParameter
import com.bineer.microservices.agentsbud.infrastructure.mapper.ContactMapper
import com.scalakka.microservices.kernel.annotation.Named
import com.scalakka.microservices.kernel.api._
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.{DataIdentity, Result}
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import org.springframework.data.domain.Page
import akka.http.scaladsl.server.Directives.{path, get => getMethod, _}
import scala.concurrent.Future

class ContactController @Inject()(@Named(classOf[ContactService]) contactServiceFactory: ActorFactory)
  extends ResourceController {

  override def post: Route = Action.postAsync {
    (data: ContactParameter, _) =>
      (contactServiceFactory() ? Save(data)).mapResult[DataIdentity]
  }

  override def put: Route = Action.putAsync {
    (id: String, data: ContactParameter, _) =>
      (contactServiceFactory() ? Update(id, data)).mapResult[DataIdentity]
  }

  override def delete: Route = Action.deleteAsync {
    (id: String, _) =>
      (contactServiceFactory() ? Delete(id)).mapResult[DataIdentity]
  }

  override def getById: Route = Action.getAsync {
    (id: String, r: Request) =>
      (contactServiceFactory() ? FindById(id, r.pageRequest)).mapResult[Option[ContactMapper]]
  }

  override def getAll: Route = Action.getAsync {
    (r: Request) =>
      (contactServiceFactory() ? FindAll(r.pageRequest)).mapResult[Page[ContactMapper]]
  }

  def getAllByUserId: Route = Action.custom {
    path(LongNumber) {
      userId: Long =>
        getMethod {
          new ActionComplete().completeAsync {
            (r: Request) =>
              (contactServiceFactory() ? FindAllByUserId(userId, r.pageRequest)).mapResult[Page[ContactMapper]]
          }
        }
    }
  }

  override def routes: Route = getAllByUserId ~ super.routes

}