package com.bineer.microservices.agentsbud.util

object RoutePathEnum {

  final val CONTACT = "contacts"
  final val CONTACT_GROUP = "contact-groups"
}