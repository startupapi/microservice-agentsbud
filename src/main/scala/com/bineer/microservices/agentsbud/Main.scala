package com.bineer.microservices.agentsbud

import com.google.inject.Injector
import com.scalakka.microservices.kernel.application.schedulers.Scheduler
import com.scalakka.microservices.kernel.domain.AggregateRepository
import com.scalakka.microservices.kernel.util.{ApiVersion, BaseMain, Handler, RouteResource}
import com.bineer.microservices.agentsbud.api.controllers.v1.{ContactController, ContactGroupController}
import com.bineer.microservices.agentsbud.application.event_handlers.{ContactEventHandler, ContactGroupEventHandler}
import com.bineer.microservices.agentsbud.domain.aggregates.ContactGroupProtocol._
import com.bineer.microservices.agentsbud.domain.aggregates.ContactProtocol._
import com.bineer.microservices.agentsbud.domain.repositories.{ContactGroupRepository, ContactRepository}
import com.bineer.microservices.agentsbud.util.RoutePathEnum._

object Main extends BaseMain with ApiVersion {

  override val injector: Injector = SystemInjector.injector

  override val routes: Seq[RouteResource] = Seq(
    RouteResource(CONTACT_GROUP) controller Map(VERSION_V1 -> classOf[ContactGroupController]),
    RouteResource(CONTACT) controller Map(VERSION_V1 -> classOf[ContactController])
  )

  override val aggregateRepositories: Seq[Class[_ <: AggregateRepository[_]]] = Seq(
    classOf[ContactRepository],
    classOf[ContactGroupRepository]
  )

  override val eventHandlers: Seq[Handler] = Seq(
    Handler(classOf[ContactGroupEventHandler]) events Seq(classOf[ContactGroupCreatedEvent], classOf[ContactGroupUpdatedEvent], classOf[ContactGroupDeletedEvent]) route RouteResource(CONTACT_GROUP),
    Handler(classOf[ContactEventHandler]) events Seq(classOf[ContactCreatedEvent], classOf[ContactUpdatedEvent], classOf[ContactDeletedEvent]) route RouteResource(CONTACT)
  )

  override val schedulers: Seq[Class[_ <: Scheduler]] = Seq()
}
