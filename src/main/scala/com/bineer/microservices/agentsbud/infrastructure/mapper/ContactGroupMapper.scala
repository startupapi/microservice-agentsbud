package com.bineer.microservices.agentsbud.infrastructure.mapper

case class ContactGroupMapper(id: Long,
                         contactGroupIdRef: String,
                         name: String,
                         userId: Long,
                         contacts: List[ContactMapper] = List.empty[ContactMapper])
