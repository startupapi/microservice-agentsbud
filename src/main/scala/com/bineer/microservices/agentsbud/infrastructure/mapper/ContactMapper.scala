package com.bineer.microservices.agentsbud.infrastructure.mapper

case class ContactMapper(id: Long,
                    contactIdRef: String,
                    userId: Long,
                    firstName: String,
                    lastName: String,
                    email: String,
                    isFavorite: Boolean,
                    isEnable: Boolean,
                    groups: List[ContactGroupMapper] = List.empty[ContactGroupMapper])
