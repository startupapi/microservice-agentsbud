package com.bineer.microservices.agentsbud.infrastructure.to;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "contact")
public class Contact {
    private int id;
    private String contactIdRef;
    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String avatar;
    private Boolean favorited;
    private Boolean enabled;
    private Set<ContactGroup> groups;

    public Contact() {
    }

    public Contact(String contactIdRef,
                   Long userId,
                   String firstName,
                   String lastName,
                   String email,
                   String avatar,
                   Boolean favorited,
                   Boolean enabled,
                   Set<ContactGroup> groups) {
        this.contactIdRef = contactIdRef;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.avatar = avatar;
        this.favorited = favorited;
        this.enabled = enabled;
        this.groups = groups;
    }

    @Column(name = "contact_id_ref")
    public String getContactIdRef() {
        return contactIdRef;
    }

    public void setContactIdRef(String contactIdRef) {
        this.contactIdRef = contactIdRef;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "favorited")
    public Boolean getFavorited() {
        return favorited;
    }

    public Boolean isFavorited() {
        return favorited ;
    }

    public void setFavorited(Boolean favorited) {
        this.favorited = favorited;
    }

    @Column(name = "enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "contact_group_contact", joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "contact_group_id", referencedColumnName = "id"))
    public Set<ContactGroup> getGroups() {
        return groups;
    }

    public void setGroups(Set<ContactGroup> groups) {
        this.groups = groups;
    }
}
