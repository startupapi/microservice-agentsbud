package com.bineer.microservices.agentsbud.infrastructure.dao

import java.util

import com.bineer.microservices.agentsbud.infrastructure.to.ContactGroup
import org.springframework.data.domain.{Page, Pageable}
import org.springframework.data.jpa.repository.JpaRepository

trait ContactGroupMysqlDao extends JpaRepository[ContactGroup, String] {

  def findByContactGroupIdRef(contactGroupIdRef: String): ContactGroup

  def findByName(name: String): ContactGroup

  def findByContactGroupIdRefIn(contactGroupIdRefs: util.Collection[String]): util.List[ContactGroup]

  def findAllByUserId(userId: Long, pageable: Pageable): Page[ContactGroup]

  def findAllByNameAndUserId(name: String, userId: Long): ContactGroup
}
