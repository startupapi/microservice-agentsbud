//package com.bineer.microservices.agentsbud.infrastructure.to
//
//import javax.persistence._
//import scala.beans.BeanProperty
//
//@Entity
//@Table(name = "contact")
//class ContactTO(contactId: String, userId: Long, firstName: String, lastName: String, email: String, isFavorite: Boolean, isEnable: Boolean) {
//
//  @Id
//  @GeneratedValue(strategy = GenerationType.AUTO)
//  @BeanProperty
//  var id: Long = _
//
//  @BeanProperty
//  @Column(name = "contact_id_ref")
//  var ci: String = contactId
//
//  @BeanProperty
//  @Column(name = "user_id")
//  var ui: Long = userId
//
//  @BeanProperty
//  @Column(name = "first_name")
//  var f: String = firstName
//
//  @BeanProperty
//  @Column(name = "last_name")
//  var l: String = lastName
//
//  @Column(name = "email")
//  @BeanProperty
//  var e: String = email
//
//  @BeanProperty
//  @Column(name = "is_favorite")
//  var isFav: Long = if (isFavorite) 1 else 0
//
//  @BeanProperty
//  @Column(name = "is_enable")
//  var isEn: Long = if (isEnable) 1 else 0
//
//  def this() = this("", -1, "", "", "", 0, 1)
//
//  def toMapper = ContactMapper(id, contactId, firstName, lastName, email, isEnable, isFavorite)
//
//}
//
//case class ContactMapper(id: Long, contactIdRef: String, firstName: String, lastName: String, email: String, isFavorite: Boolean, isEnable: Boolean)
//
