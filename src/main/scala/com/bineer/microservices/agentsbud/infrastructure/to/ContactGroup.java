package com.bineer.microservices.agentsbud.infrastructure.to;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "contact_group")
public class ContactGroup {
    private int id;
    private String contactGroupIdRef;
    private String name;
    private Long userId;
    private Set<Contact> contacts;

    public ContactGroup() {
    }

    public ContactGroup(String contactGroupIdRef,String name, Long userId) {
        this.name = name;
        this.contactGroupIdRef = contactGroupIdRef;
        this.userId = userId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "contact_group_id_ref")
    public String getContactGroupIdRef() {
        return contactGroupIdRef;
    }

    public void setContactGroupIdRef(String contactGroupIdRef) {
        this.contactGroupIdRef = contactGroupIdRef;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @ManyToMany(mappedBy = "groups")
    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contact) {
        this.contacts = contact;
    }
}
