package com.bineer.microservices.agentsbud.infrastructure

import com.google.inject.{AbstractModule, Provider}
import com.scalakka.microservices.kernel.util.{EnvironmentConfig, StringUtil}
import com.bineer.microservices.agentsbud.infrastructure.dao.{ContactGroupMysqlDao, ContactMysqlDao}
import net.codingwell.scalaguice.ScalaModule
import org.springframework.context.ApplicationContext

package object ioc {

  object InfrastructureModule {
    private val context: ApplicationContext = EnvironmentConfig.getApplicationContext("agentsbud-repository-jpa")

    class ContactMysqlDaoProvider() extends Provider[ContactMysqlDao] {
      override def get(): ContactMysqlDao = {
        context.getBean(StringUtil.decapitalize(classOf[ContactMysqlDao])).asInstanceOf[ContactMysqlDao]
      }
    }

    class ContactGroupMysqlDaoProvider() extends Provider[ContactGroupMysqlDao] {
      override def get(): ContactGroupMysqlDao = {
        context.getBean(StringUtil.decapitalize(classOf[ContactGroupMysqlDao])).asInstanceOf[ContactGroupMysqlDao]
      }
    }

  }

  class InfrastructureModule extends AbstractModule with ScalaModule {
    override def configure(): Unit = {
      bind[ContactMysqlDao].toProvider[InfrastructureModule.ContactMysqlDaoProvider]
      bind[ContactGroupMysqlDao].toProvider[InfrastructureModule.ContactGroupMysqlDaoProvider]
    }
  }

}
