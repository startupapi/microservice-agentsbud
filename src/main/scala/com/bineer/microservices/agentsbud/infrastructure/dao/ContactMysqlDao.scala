package com.bineer.microservices.agentsbud.infrastructure.dao

import com.bineer.microservices.agentsbud.infrastructure.to.Contact
import org.springframework.data.domain.{Page, Pageable}
import org.springframework.data.jpa.repository.JpaRepository

trait ContactMysqlDao extends JpaRepository[Contact, String] {

  def findByContactIdRef(contactIdRef: String): Contact

  def findByEmail(email: String): Contact

  def findAllByUserId(userId: Long, pageable: Pageable): Page[Contact]
}
