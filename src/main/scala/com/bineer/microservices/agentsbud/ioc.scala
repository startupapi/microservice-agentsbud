package com.bineer.microservices.agentsbud

import akka.actor.ActorSystem
import com.google.inject.{AbstractModule, Guice, Injector}
import com.scalakka.microservices.kernel.KernelModule
import com.bineer.microservices.agentsbud.application.ioc._
import com.bineer.microservices.agentsbud.domain.ioc._
import com.bineer.microservices.agentsbud.infrastructure.ioc._
import net.codingwell.scalaguice.ScalaModule

object SystemModule {
  val system: ActorSystem = KernelModule.system
}

class SystemModule extends AbstractModule with ScalaModule {
  override def configure() {}
}

object SystemInjector {
  val injector: Injector = Guice.createInjector(new SystemModule, new KernelModule, new ApplicationModule, new DomainModule, new InfrastructureModule)
}
