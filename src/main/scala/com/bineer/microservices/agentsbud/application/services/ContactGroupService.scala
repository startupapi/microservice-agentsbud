package com.bineer.microservices.agentsbud.application.services

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.{ask, pipe}
import com.bineer.microservices.agentsbud.SystemInjector
import com.bineer.microservices.agentsbud.domain.aggregates.ContactGroupProtocol._
import com.bineer.microservices.agentsbud.domain.repositories.ContactGroupRepository
import com.bineer.microservices.agentsbud.domain.util.ContactGroupParameter
import com.bineer.microservices.agentsbud.infrastructure.dao.ContactGroupMysqlDao
import com.bineer.microservices.agentsbud.infrastructure.mapper.{ContactGroupMapper, ContactMapper}
import com.bineer.microservices.agentsbud.infrastructure.to.ContactGroup
import com.scalakka.microservices.kernel.annotation.Names
import com.scalakka.microservices.kernel.domain.AggregateRepositoryProtocol.{CreateAggregate, FindByIdAggregate}
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import com.scalakka.microservices.kernel.util._
import net.codingwell.scalaguice.InjectorExtensions._
import org.springframework.data.domain.{Page, PageImpl}
import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.util.{Try, Failure => TryFailure, Success => TrySuccess}

class ContactGroupService extends Actor with ActorLogging {

  import ContactGroupServiceMessageProtocol._
  import ContactGroupServiceProtocol._

  val userRepositoryFactory: ActorFactory = SystemInjector.injector.instance[ActorFactory](Names.named(classOf[ContactGroupRepository]))
  val contactGroupMysqlDao: ContactGroupMysqlDao = SystemInjector.injector.instance[ContactGroupMysqlDao]

  override def receive: Receive = {
    case Save(data: ContactGroupParameter) => save(data: ContactGroupParameter) pipeTo sender()
    case Update(id, data: ContactGroupParameter) => update(id, data)
    case Delete(id) => delete(id) pipeTo sender()
    case FindAll(pageRequest) => sender() ! findAll(pageRequest)
    case FindById(id, _) => sender() ! findById(id)
    case FindAllByUserId(userId, pageRequest) => sender() ! findAllByUserId(userId, pageRequest)
  }

  def save(data: ContactGroupParameter): AsyncResult[DataIdentity] =
    Option(contactGroupMysqlDao.findAllByNameAndUserId(data.name, data.userId)) match {
      case None => (userRepositoryFactory() ? CreateAggregate(CreateContactGroupCommand(IdentifierGenerator.generate, data)))
        .mapResult[DataIdentity]
      case _ => Future(Failure(List(AlreadyExistContactGroupError)))
    }

  def update(id: String, data: ContactGroupParameter): AsyncResult[DataIdentity] =
    Option(contactGroupMysqlDao.findByName(data.name)) match {
      case None => (userRepositoryFactory() ? FindByIdAggregate(UpdateContactGroupCommand(id, data)))
        .mapResult[DataIdentity]
      case _ => Future(Failure(List(AlreadyExistContactGroupError)))
    }

  def delete(id: String): AsyncResult[DataIdentity] = {
    (userRepositoryFactory() ? FindByIdAggregate(DeleteContactGroupCommand(id))).mapResult[DataIdentity]
  }

  def findAll(pageRequest: PageRequest): Result[Page[ContactGroupMapper]] = {
    Try(contactGroupMysqlDao.findAll(pageRequest)) match {
      case TrySuccess(pageContactGroup: Page[ContactGroup]) =>
        implicit val pr: PageRequest = pageRequest
        Result(pageContactGroup)
      case TryFailure(e) =>
        log.error(s"::::::::::: [Contact Group Service][findAll] -> error = ${e.getMessage} :::::::::::::")
        Failure(List(OperationDaoError))
    }
  }

  def findById(id: String): Result[Option[ContactGroupMapper]] = {
    Try(Option(contactGroupMysqlDao.findByContactGroupIdRef(id))) match {
      case TrySuccess(Some(contactGroup)) => Result(Some(contactGroup))
      case TrySuccess(_) => Failure(List(NotFountContactGroupError))
      case TryFailure(e) =>
        log.error(s"::::::::::: [Contact Group Service][findById] -> error = ${e.getMessage} :::::::::::::")
        Failure(List(OperationDaoError))
    }
  }

  def findAllByUserId(userId: Long, pageRequest: PageRequest): Result[Page[ContactGroupMapper]] = {
    Try(contactGroupMysqlDao.findAllByUserId(userId, pageRequest)) match {
      case TrySuccess(pageContactGroup: Page[ContactGroup]) =>
        implicit val pr: PageRequest = pageRequest
        Result(pageContactGroup)
      case TrySuccess(_) => Failure(List(NotFountContactGroupError))
      case TryFailure(e) =>
        log.error(s"::::::::::: [Contact Group Service][findAllByUserId] -> error = ${e.getMessage} :::::::::::::")
        Failure(List(OperationDaoError))
    }
  }

  implicit def contactGroupPage2ContactGroupMapperPage(page: Page[ContactGroup])(implicit pageRequest: PageRequest): Page[ContactGroupMapper] = {
    val content: List[ContactGroupMapper] = page.toList.map(item =>
      ContactGroupMapper(
        item.getId,
        item.getContactGroupIdRef,
        item.getName,
        item.getUserId,
        item.getContacts.toList.map(c => ContactMapper(
          c.getId,
          c.getContactIdRef,
          c.getUserId,
          c.getFirstName,
          c.getLastName,
          c.getEmail,
          c.isFavorited,
          c.isEnabled
        ))
      )
    )
    new PageImpl(content, pageRequest, page.getTotalElements)
  }

  implicit def contactGroup2ContactGroupMapper(contactGroup: ContactGroup): ContactGroupMapper = ContactGroupMapper(
    contactGroup.getId,
    contactGroup.getContactGroupIdRef,
    contactGroup.getName,
    contactGroup.getUserId,
    contactGroup.getContacts.toList.map(c => ContactMapper(
      c.getId,
      c.getContactIdRef,
      c.getUserId,
      c.getFirstName,
      c.getLastName,
      c.getEmail,
      c.isFavorited,
      c.isEnabled
    ))
  )
}

object ContactGroupService {
  def props = Props(classOf[ContactGroupService])
}

object ContactGroupServiceMessageProtocol {

  case object AlreadyExistContactGroupError extends Error

  case object NotFountContactGroupError extends Error

  case object OperationDaoError extends Error

}

object ContactGroupServiceProtocol extends CrudProtocol {

  case class FindAllByUserId(userId: Long, pageRequest: PageRequest)

}
