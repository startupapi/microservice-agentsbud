package com.bineer.microservices.agentsbud.application

import com.google.inject.{AbstractModule, Provider}
import com.scalakka.microservices.kernel.annotation.{Named, Names}
import com.scalakka.microservices.kernel.application.ServiceProvider
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.bineer.microservices.agentsbud.SystemModule
import com.bineer.microservices.agentsbud.application.services.{ContactGroupService, ContactService}
import net.codingwell.scalaguice.ScalaModule

package object ioc {

  object ApplicationModule {

    @Named(classOf[ContactService])
    class ContactServiceProvider() extends Provider[ActorFactory] with ServiceProvider {
      override def get(): ActorFactory = () => SystemModule.system.actorOf(ContactService.props, generateName)
    }

    @Named(classOf[ContactGroupService])
    class ContactGroupServiceProvider() extends Provider[ActorFactory] with ServiceProvider {
      override def get(): ActorFactory = () => SystemModule.system.actorOf(ContactGroupService.props, generateName)
    }

  }

  class ApplicationModule extends AbstractModule with ScalaModule {
    override def configure(): Unit = {
      bind[ActorFactory].annotatedWith(Names.named(classOf[ContactService])).toProvider[ApplicationModule.ContactServiceProvider]
      bind[ActorFactory].annotatedWith(Names.named(classOf[ContactGroupService])).toProvider[ApplicationModule.ContactGroupServiceProvider]
    }
  }

}

