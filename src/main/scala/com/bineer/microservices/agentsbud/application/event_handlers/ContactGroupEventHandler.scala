package com.bineer.microservices.agentsbud.application.event_handlers

import com.bineer.microservices.agentsbud.SystemInjector
import com.bineer.microservices.agentsbud.domain.aggregates.ContactGroupProtocol._
import com.bineer.microservices.agentsbud.domain.util.ContactGroupParameter
import com.bineer.microservices.agentsbud.infrastructure.dao.ContactGroupMysqlDao
import com.bineer.microservices.agentsbud.infrastructure.to.ContactGroup
import com.scalakka.microservices.kernel.application.event_handlers.EventHandler
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol.IdentifiedEvent
import net.codingwell.scalaguice.InjectorExtensions._

class ContactGroupEventHandler extends EventHandler {

  val contactGroupMysqlDao: ContactGroupMysqlDao = SystemInjector.injector.instance[ContactGroupMysqlDao]

  override val eventMatching: PartialFunction[IdentifiedEvent, Unit] = {
    case ContactGroupCreatedEvent(id, data) => saveContactGroup(id, data)
    case ContactGroupUpdatedEvent(id, data) => updateContactGroup(id, data)
    case ContactGroupDeletedEvent(id) => deleteContactGroup(id)
  }

  def saveContactGroup(id: String, data: ContactGroupParameter): Unit = {
    contactGroupMysqlDao.save(new ContactGroup(id, data.name, data.userId))
  }

  def updateContactGroup(id: String, data: ContactGroupParameter): Unit = Option(contactGroupMysqlDao.findByContactGroupIdRef(id)) match {
    case Some(contactGroup) =>
      contactGroup.setName(data.name)
      contactGroupMysqlDao.save(contactGroup)
    case None =>
  }

  def deleteContactGroup(id: String): Unit = Option(contactGroupMysqlDao.findByContactGroupIdRef(id)) match {
    case Some(user) => contactGroupMysqlDao.delete(user)
    case None =>
  }

}
