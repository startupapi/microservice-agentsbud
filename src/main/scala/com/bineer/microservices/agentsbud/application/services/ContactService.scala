package com.bineer.microservices.agentsbud.application.services

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.{ask, pipe}
import com.bineer.microservices.agentsbud.SystemInjector
import com.bineer.microservices.agentsbud.domain.aggregates.ContactProtocol._
import com.bineer.microservices.agentsbud.domain.repositories.ContactRepository
import com.bineer.microservices.agentsbud.domain.util.ContactParameter
import com.bineer.microservices.agentsbud.infrastructure.dao.ContactMysqlDao
import com.bineer.microservices.agentsbud.infrastructure.mapper.{ContactGroupMapper, ContactMapper}
import com.bineer.microservices.agentsbud.infrastructure.to.{Contact, ContactGroup}
import com.scalakka.microservices.kernel.annotation.Names
import com.scalakka.microservices.kernel.domain.AggregateRepositoryProtocol.{CreateAggregate, FindByIdAggregate}
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.Result._
import com.scalakka.microservices.kernel.util.Timeout.Implicit._
import com.scalakka.microservices.kernel.util._
import net.codingwell.scalaguice.InjectorExtensions._
import org.springframework.data.domain.{Page, PageImpl}
import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.util.Try
import scala.util.{Failure => TryFailure, Success => TrySuccess}

class ContactService extends Actor with ActorLogging {

  import ContactServiceMessageProtocol._
  import ContactServiceProtocol._

  val userRepositoryFactory: ActorFactory = SystemInjector.injector.instance[ActorFactory](Names.named(classOf[ContactRepository]))
  val userDao: ContactMysqlDao = SystemInjector.injector.instance[ContactMysqlDao]

  override def receive: Receive = {
    case Save(data: ContactParameter) => save(data: ContactParameter) pipeTo sender()
    case Update(id, data: ContactParameter) => update(id, data) pipeTo sender()
    case Delete(id) => delete(id) pipeTo sender()
    case FindAllByUserId(userId, pageRequest) => sender() ! findAllByUserId(userId, pageRequest)
  }

  def save(data: ContactParameter): AsyncResult[DataIdentity] = Option(userDao.findByEmail(data.email)) match {
    case None => (userRepositoryFactory() ? CreateAggregate(CreateContactCommand(IdentifierGenerator.generate, data))).mapResult[DataIdentity]
    case _ => Future(Failure(List(EmailUserRegisteredError)))
  }

  def update(id: String, data: ContactParameter): AsyncResult[DataIdentity] = {
    (userRepositoryFactory() ? FindByIdAggregate(UpdateContactCommand(id, data))).mapResult[DataIdentity]
  }

  def delete(id: String): AsyncResult[DataIdentity] = {
    (userRepositoryFactory() ? FindByIdAggregate(DeleteContactCommand(id))).mapResult[DataIdentity]
  }

  def findAllByUserId(userId: Long, pageRequest: PageRequest): Result[Page[ContactMapper]] = {
    Try(userDao.findAllByUserId(userId, pageRequest)) match {
      case TrySuccess(pageContact) =>
        implicit val pr: PageRequest = pageRequest
        Result(pageContact)
      case TrySuccess(_) => Failure(List(NotFountContactError))
      case TryFailure(e) =>
        log.error(s"::::::::::: [Contact Group Service][findAllByUserId] -> error = ${e.getMessage} :::::::::::::")
        Failure(List(OperationDaoError))
    }
  }

  implicit def contactPage2ContactMapperPage(page: Page[Contact])(implicit pageRequest: PageRequest): Page[ContactMapper] = {
    val content: List[ContactMapper] = page.toList.map(item =>
      ContactMapper(
        item.getId,
        item.getContactIdRef,
        item.getUserId,
        item.getFirstName,
        item.getLastName,
        item.getEmail,
        item.getFavorited,
        item.getEnabled,
        item.getGroups.toList.map(cg => ContactGroupMapper(cg.getId, cg.getContactGroupIdRef, cg.getName, cg.getUserId))
      )
    )
    new PageImpl(content, pageRequest, page.getTotalElements)
  }

}

object ContactService {
  def props = Props(classOf[ContactService])
}

object ContactServiceMessageProtocol {

  case object EmailUserRegisteredError extends Error

  case object NotFountContactError extends Error

  case object OperationDaoError extends Error

}

object ContactServiceProtocol extends CrudProtocol {

  case class FindAllByUserId(userId: Long, pageRequest: PageRequest)

}
