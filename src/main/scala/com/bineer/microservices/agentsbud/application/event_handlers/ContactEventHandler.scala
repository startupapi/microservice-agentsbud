package com.bineer.microservices.agentsbud.application.event_handlers

import com.bineer.microservices.agentsbud.SystemInjector
import com.bineer.microservices.agentsbud.domain.aggregates.ContactProtocol._
import com.bineer.microservices.agentsbud.domain.util.ContactParameter
import com.bineer.microservices.agentsbud.infrastructure.dao.{ContactGroupMysqlDao, ContactMysqlDao}
import com.bineer.microservices.agentsbud.infrastructure.to.{Contact, ContactGroup}
import com.scalakka.microservices.kernel.application.event_handlers.EventHandler
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol.IdentifiedEvent
import net.codingwell.scalaguice.InjectorExtensions._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

class ContactEventHandler extends EventHandler {

  val contactMysqlDao: ContactMysqlDao = SystemInjector.injector.instance[ContactMysqlDao]
  val contactGroupMysqlDao: ContactGroupMysqlDao = SystemInjector.injector.instance[ContactGroupMysqlDao]

  override val eventMatching: PartialFunction[IdentifiedEvent, Unit] = {
    case ContactCreatedEvent(id, data) => saveContact(id, data)
    case ContactUpdatedEvent(id, data) => updateContact(id, data)
    case ContactDeletedEvent(id) => deleteContact(id)
  }

  def saveContact(id: String, data: ContactParameter): Unit = {
    var contactGroups: Set[ContactGroup] = Set.empty[ContactGroup]
    if (data.groups.nonEmpty) contactGroups = contactGroupMysqlDao.findByContactGroupIdRefIn(data.groups.asJava).toList.toSet
    contactMysqlDao.save(new Contact(
      id,
      data.userId,
      data.firstName,
      data.lastName,
      data.email,
      getAvatar(data.avatar),
      data.favorited,
      data.enabled,
      contactGroups.asJava
    ))
  }

  def updateContact(id: String, data: ContactParameter): Unit = Option(contactMysqlDao.findByContactIdRef(id)) match {
    case Some(contact) =>
      var contactGroups: Set[ContactGroup] = Set.empty[ContactGroup]
      if (data.groups.nonEmpty) contactGroups = contactGroupMysqlDao.findByContactGroupIdRefIn(data.groups.asJava).toList.toSet
      contact.setUserId(data.userId)
      contact.setFirstName(data.firstName)
      contact.setLastName(data.lastName)
      contact.setEmail(data.email)
      contact.setAvatar(getAvatar(data.avatar))
      contact.setFavorited(data.favorited)
      contact.setEnabled(data.enabled)
      contact.setGroups(contactGroups.asJava)
      contactMysqlDao.save(contact)
    case None =>
  }

  def getAvatar(avatar: Option[String]): String = avatar match {
    case Some(value) => value
    case _ => ""
  }

  def deleteContact(id: String): Unit = Option(contactMysqlDao.findByContactIdRef(id)) match {
    case Some(contact) => contactMysqlDao.delete(contact)
    case None =>
  }

}
