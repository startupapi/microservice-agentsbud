package com.bineer.microservices.agentsbud.domain.aggregates

import com.bineer.microservices.agentsbud.domain.util.ContactGroupParameter
import com.scalakka.microservices.kernel.domain.AggregateRoot
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol._
import com.scalakka.microservices.kernel.util.Result.Error
import com.scalakka.microservices.kernel.util._

class ContactGroup extends AggregateRoot {

  import ContactGroupMessageProtocol._
  import ContactGroupProtocol._

  override def validateCommand: PartialFunction[Command, Result[AggregateRootResponse]] = {
    case CreateContactGroupCommand(_, data) if data.name.isEmpty => Failure(List(NameIsEmpty))
    case CreateContactGroupCommand(id, data) => Success(AggregateRootResponse(DataIdentity(id), Some(ContactGroupCreatedEvent(id, data))))
    case UpdateContactGroupCommand(_, data) if data.name.isEmpty => Failure(List(NameIsEmpty))
    case UpdateContactGroupCommand(id, data) => Success(AggregateRootResponse(DataIdentity(id), Some(ContactGroupUpdatedEvent(id, data))))
    case DeleteContactGroupCommand(id) => Success(AggregateRootResponse(DataIdentity(id), Some(ContactGroupDeletedEvent(id))))
  }
}

object ContactGroupMessageProtocol {

  case object NameIsEmpty extends Error

}

object ContactGroupProtocol {

  case class CreateContactGroupCommand(aggregateId: String, data: ContactGroupParameter) extends CreateCommand

  case class UpdateContactGroupCommand(aggregateId: String, data: ContactGroupParameter) extends IdentifiedCommand

  case class DeleteContactGroupCommand(aggregateId: String) extends DeleteCommand

  case class ContactGroupCreatedEvent(id: String, data: ContactGroupParameter) extends CreatedEvent

  case class ContactGroupUpdatedEvent(id: String, data: ContactGroupParameter) extends UpdatedEvent

  case class ContactGroupDeletedEvent(id: String) extends DeletedEvent

}
