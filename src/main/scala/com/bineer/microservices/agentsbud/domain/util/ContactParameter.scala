package com.bineer.microservices.agentsbud.domain.util

import com.scalakka.microservices.kernel.api.Parameter

case class ContactParameter(userId: Long,
                            firstName: String,
                            lastName: String,
                            email: String,
                            avatar: Option[String],
                            favorited: Boolean,
                            enabled: Boolean,
                            groups: List[String]) extends Parameter

