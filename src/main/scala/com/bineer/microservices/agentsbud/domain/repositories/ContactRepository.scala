package com.bineer.microservices.agentsbud.domain.repositories

import com.scalakka.microservices.kernel.domain.AggregateRepository
import com.bineer.microservices.agentsbud.domain.aggregates.Contact

class ContactRepository extends AggregateRepository[Contact]
