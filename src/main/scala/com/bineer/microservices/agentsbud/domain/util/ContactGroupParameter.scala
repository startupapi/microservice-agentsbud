package com.bineer.microservices.agentsbud.domain.util

import com.scalakka.microservices.kernel.api.Parameter

case class ContactGroupParameter(name: String, userId: Long) extends Parameter

