package com.bineer.microservices.agentsbud.domain.repositories

import com.bineer.microservices.agentsbud.domain.aggregates.ContactGroup
import com.scalakka.microservices.kernel.domain.AggregateRepository

class ContactGroupRepository extends AggregateRepository[ContactGroup]
