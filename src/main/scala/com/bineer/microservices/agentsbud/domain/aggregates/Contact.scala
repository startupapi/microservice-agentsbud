package com.bineer.microservices.agentsbud.domain.aggregates

import com.bineer.microservices.agentsbud.domain.util.ContactParameter
import com.scalakka.microservices.kernel.domain.AggregateRoot
import com.scalakka.microservices.kernel.domain.AggregateRootProtocol._
import com.scalakka.microservices.kernel.util.Result.Error
import com.scalakka.microservices.kernel.util._

class Contact extends AggregateRoot {

  import ContactMessageProtocol._
  import ContactProtocol._

  override def validateCommand: PartialFunction[Command, Result[AggregateRootResponse]] = {
    case CreateContactCommand(id, data) => createUser(id, data, validateFields(data, isUpdate = false))
    case UpdateContactCommand(id, data) => updateUser(id, data, validateFields(data))
    case DeleteContactCommand(id) => Success(AggregateRootResponse(DataIdentity(id), Some(ContactDeletedEvent(id))))
  }

  def createUser(id: String, data: ContactParameter, listError: List[Error]): Result[AggregateRootResponse] = listError match {
    case Nil => Success(AggregateRootResponse(DataIdentity(id), Some(ContactCreatedEvent(id, data))))
    case _ => Failure(listError)
  }

  def updateUser(id: String, data: ContactParameter, listError: List[Error]): Result[AggregateRootResponse] = listError match {
    case Nil => Success(AggregateRootResponse(DataIdentity(id), Some(ContactUpdatedEvent(id, data))))
    case _ => Failure(listError)
  }

  def validateFields(data: ContactParameter, isUpdate: Boolean = true): List[Error] = {
    var listError = List.empty[Error]
    if (data.firstName.isEmpty) listError = listError ::: List(FirstNameIsEmpty)
    if (data.lastName.isEmpty) listError = listError ::: List(LastNameIsEmpty)
    if ((data.email.isEmpty || data.email.isEmpty) && !isUpdate) listError = listError ::: List(EmailIsEmpty)
    listError
  }
}

object ContactMessageProtocol {

  case object FirstNameIsEmpty extends Error

  case object LastNameIsEmpty extends Error

  case object EmailIsEmpty extends Error

}

object ContactProtocol {

  case class CreateContactCommand(aggregateId: String, data: ContactParameter) extends CreateCommand

  case class UpdateContactCommand(aggregateId: String, data: ContactParameter) extends IdentifiedCommand

  case class DeleteContactCommand(aggregateId: String) extends DeleteCommand

  case class ContactCreatedEvent(id: String, data: ContactParameter) extends CreatedEvent

  case class ContactUpdatedEvent(id: String, data: ContactParameter) extends UpdatedEvent

  case class ContactDeletedEvent(id: String) extends DeletedEvent

}
