package com.bineer.microservices.agentsbud.domain

import com.google.inject._
import com.scalakka.microservices.kernel.annotation.{Named, Names}
import com.scalakka.microservices.kernel.application.ServiceProvider
import com.scalakka.microservices.kernel.util.ActorFactoryType.ActorFactory
import com.scalakka.microservices.kernel.util.ClusterShardingManager
import com.bineer.microservices.agentsbud.domain.repositories.{ContactGroupRepository, ContactRepository}
import net.codingwell.scalaguice.ScalaModule

 package object  ioc {

   object DomainModule {

     @Named(classOf[ContactRepository])
     class ContactRepositoryProvider() extends Provider[ActorFactory] with ServiceProvider {
       override def get(): ActorFactory = () => ClusterShardingManager.getActor(classOf[ContactRepository])
     }

     @Named(classOf[ContactRepository])
     class ContactGroupRepositoryProvider() extends Provider[ActorFactory] with ServiceProvider {
       override def get(): ActorFactory = () => ClusterShardingManager.getActor(classOf[ContactGroupRepository])
     }

   }

   class DomainModule extends AbstractModule with ScalaModule {
     override def configure(): Unit = {
       bind[ActorFactory].annotatedWith(Names.named(classOf[ContactRepository])).toProvider[DomainModule.ContactRepositoryProvider]
       bind[ActorFactory].annotatedWith(Names.named(classOf[ContactGroupRepository])).toProvider[DomainModule.ContactGroupRepositoryProvider]
     }
   }
}
